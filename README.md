# PHP Minify CLI

Command line tool to minify CSS and JS

## Installation with composer

```bash

composer global require alc/php-minify-cli

# Make sure you have export PATH in your ~/.bashrc
export PATH=~/.config/composer/vendor/bin:$PATH

```

## Usage

```bash

# Minify CSS
php-minify-cli minify main.css main.min.css

# Minify JS
php-minify-cli minify main.js main.min.js

```
