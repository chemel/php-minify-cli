<?php

namespace Alc\MinifyCli\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use MatthiasMullie\Minify;

class MinifyCommand extends Command
{
    protected function configure()
    {
	    $this
	        ->setName('minify')
	        ->setDescription('Minify a file')
	        ->addArgument('input', InputArgument::REQUIRED, 'The input file.')
	        ->addArgument('output', InputArgument::REQUIRED, 'The output file.')
	    ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
    	$inputFilename = $input->getArgument('input');
        $outputFilename = $input->getArgument('output');

        $inputFilename = new \SplFileInfo($inputFilename);
        $outputFilename = new \SplFileInfo($outputFilename);

        // Check if file exist
        if (!$inputFilename->isFile()) {
            $output->writeln('<error>Input file doesn\'t exist<error>');
            exit();
        }

        // Get minifier
        if($inputFilename->getExtension() == 'css') {
            $minifier = new Minify\CSS($inputFilename->getRealPath());
        }
        elseif ($inputFilename->getExtension() == 'js') {
            $minifier = new Minify\JS($inputFilename->getRealPath());
        }
        else {
            $output->writeln('<error>Can\'t minify a '.$inputFilename->getExtension().' file<error>');
            exit();
        }

        // Minify the file
        $minifier->minify($outputFilename->getPathname());

        // Show success
        $output->writeLn('<info>[Success]</info> Minify: '.$inputFilename->getBasename());
    }
}
